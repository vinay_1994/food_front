import React, { Component } from 'react';
import { Button, notification, AutoComplete } from 'antd';
import socketio from 'socket.io-client';


class notifications extends Component {

    constructor(props){
        super(props);
        this.state={
            message:[]
        }
    }


    componentDidMount = () => {
      this.io=socketio('http://192.168.3.57:3002');

      this.io.emit("join",{data:"some data from client"})
        
      this.io.on("server",msg=>{
          console.log(msg)
          this.setState({
              message:[msg, ...this.state.message]
          })
      })
  
    }
    

  render() {
      console.log(this.state)
/* 
    const msg = this.state.message.map((m,b)=>{
        return <li key={b}><p>{m}</p></li>
    }) */


    const close = () => {
        console.log('Notification was closed. Either the close button was clicked or duration time elapsed.');
      };
      
      const openNotification = () => {
        const key = `open${Date.now()}`;
        const btn = (
            <div>
                <Button type="primary" size="small" onClick={() => notification.close(key)}>
                    Confirm
                </Button>
                <Button type="primary" size="small" onClick={() => notification.close(key)}>
                Confirm
                </Button>
            </div>
        );
        notification.open({
          message: 'Notification Title ',
          description: "kljvbrkb",
          btn,
          key,
          onClose: close,
          duration:29,
          style:{
              position:"relative",
              height:AutoComplete
          },
          top:0
        });
      };


    return (
      <div>
        <Button type="primary" onClick={openNotification}>
            Open the notification box
        </Button>
      </div>
    )
  }
}

export default notifications
