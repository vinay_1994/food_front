import React from 'react';
import { Link , NavLink } from 'react-router-dom';
import { Button } from 'antd';


const sideBar = (props)=> {
  //const data = props.data.location.state;
  return (
    <div>
      <ul>
          <li><Link to={{pathname:"/liveOrders"}} >
            <Button type="primary">LIVE ORDERS</Button>
            </Link>
          </li>
          <li><Link to="/">
            <Button type="primary">MANAGE </Button>
          </Link></li>
          <li><Link to="/"><Button type="primary">PROFILE</Button></Link></li>
      </ul>
    </div>
  )
}

//export default sideBar;