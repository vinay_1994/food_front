import React, { Component } from 'react';
import { Layout, Menu, Icon, Button, Avatar, Popover  } from 'antd';
import { Route, BrowserRouter, Link } from 'react-router-dom';
import socketio from 'socket.io-client';
import URL from '../api';
import JWT from './../common/tokenStore';


import LiveOrders from './liveOrdersComponent';
import OrderDetails from './Orders/ordersComponent'


import './Style.css'


export default class CatereDashboardNew extends Component {

    constructor(props){
        super(props);
        this.state={
            CAT_ID : "",
            USER_NAME : "",
            AREA : "",
            ROLE : ""
           
        }
    }
    async componentDidMount() {
      const token = JWT.decodeToken(JWT.getToken())
       /*  await this.setState({
            CAT_ID : token.Caterer_Id,
            USER_NAME : token.user,
            AREA : token.Area,
            ROLE : token.role
        }) */
    }
    
    joinNamespace = ()=>{
      this.io = socketio(URL.BASE_URL.concat('client'));
    }

    render() {

        const {
            Header, Content, Footer, Sider,
        } = Layout;

        const popOverContent = (
            <Button onClick={()=>{
                sessionStorage.clear()
                window.location.reload()
            }}>Logout</Button>
        )
        return (
            
            <BrowserRouter>
                <Layout>
                    <Sider
                        breakpoint="lg"
                        collapsedWidth="0"
                        onBreakpoint={(broken) => { console.log(broken); }}
                        onCollapse={(collapsed, type) => { console.log(collapsed, type); }}
                    >
                        <div className="logo" />
                        <Menu theme="dark" mode="inline" defaultSelectedKeys={['4']}>
                            <Menu.Item key="1">
                                <Icon type="api" />
                                <span className="nav-text">
                                <Link to={{
                                    pathname : "/dashboard/liveOrders",
                                    state : this.state
                                    }}>
                                    <Button type="dashed" onClick = {this.joinNamespace}>Connect</Button>
                                </Link>
                                </span>
                            </Menu.Item>
                            <Menu.Item key="2">
                                <Icon type="dollar" theme="twoTone"/>
                                <span className="nav-text">
                                <Link to="/dashboard/orderDetails">Orders
                                </Link>
                                </span>
                            </Menu.Item>
                            <Menu.Item key="3">
                                <Icon type="upload" />
                                <span className="nav-text">nav 3</span>
                            </Menu.Item>
                            <Menu.Item key="4">
                                <Icon type="user" />
                                <span className="nav-text">nav 4</span>
                            </Menu.Item>
                        </Menu>
                    </Sider>
                    <Layout>
                        <Header>
                            
                        <Popover content={popOverContent} title="Business Name" placement="bottom" arrowPointAtCenter>
                            <Avatar size={50} icon="user" />
                        </Popover>
                                
                    
                        </Header>
                        <Content style={{ margin: '24px 16px 0' }}>
                            <div style={{ padding: 24, background: '#fff', minHeight: 680 }}>
                                <Route path="/dashboard/liveOrders" component={LiveOrders} />
                                <Route path="/dashboard/orderDetails" component={OrderDetails} />

                             </div>
                        </Content>
                        <Footer style={{ textAlign: 'center' }}>
                            Pavan Technologies ©2019 Created by NANU
                        </Footer>
                    </Layout>
                </Layout>
        </BrowserRouter>
        )
    }
}
