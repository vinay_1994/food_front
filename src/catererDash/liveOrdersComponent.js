import React, { Component } from 'react';
import { Badge, Modal, Button, Input } from 'antd';
import URL from '../api';


//import socket io
import socketio from 'socket.io-client';

//styles
import './Style.css'



 class liveOrdersComponent extends Component {

     constructor(props){
        super(props);
        this.state={
            notification:false,
            CAT_ID:"",
            USER_NAME:"",
            AREA:"" ,
            badge : false,
            button : false,
            modalVisible : false,
            userarea : "",
            items : "",
            quantity : "",
            address : "",
            rate : "",
            socket_id:"",
            amount : ""
        }
  }

  async componentWillMount(){
    const data = this.props.location.state
    this.io = socketio(URL.BASE_URL.concat('client'));
    await this.setState({
      CAT_ID : data.CAT_ID,
      USER_NAME : data.USER_NAME,
      AREA : data.AREA
    })
  }

  

  joinRoom = () =>{
      const name=this.state.USER_NAME;
      const CAT_ID=this.state.CAT_ID;
      const AREA = this.state.AREA; 
      this.io.emit('JOIN_ROOM',{AREA,CAT_ID,name});

      //badge confirmation for joined room
      this.io.on("ROOM_JOINED_CONFIRMATION",()=>{
        this.setState({
          badge : true,
          button : true
        })
      })
  }

  leaveRoom = () =>{
    const AREA = this.state.AREA;

    this.io.emit("LEAVE_ROOM",{AREA});
    this.setState({
      badge : false,
      button : false
    })
  }

  acceptOrder = ()=>{
    console.log(this.state.socket_id)
    const { CAT_ID , USER_NAME, socket_id ,amount} = this.state
    this.io.emit("ACCEPT_ORDER",{CAT_ID,USER_NAME,socket_id,amount});
    this.setState({
      modalVisible : false
    })
  }

  cancelOrder = ()=>{
    this.setState({
      modalVisible : false
    })
  }

  acceptamount = (e) =>{
    this.setState({
      amount : e.target.value
    })
    console.log(this.state.amount)
  }
  

  render(){


    const badge = this.state.badge;
    const button = this.state.button;
    const { userarea,address,quantity,items,rate } = this.state
    
    this.io.on("WAITING_ORDER",(data)=>{
      
        this.setState({
          modalVisible : true,
          address:data.address,
          userarea : data.area,
          items : data.item,
          quantity : data.quantity,
          rate : data.rate,
          socket_id:data.id
        })

      setTimeout(()=>this.setState({modalVisible : false}),60000)
    })


    

    return(
      <div className = "live_order">
        {
          button ? <Button className = "live_order_button" type="primary" icon="notification" size="large" onClick = {this.leaveRoom}>Log out from live orders</Button> : <Button type="primary" icon="notification" size="large" onClick = {this.joinRoom}>CLick Hear to Get Live Orders</Button>
        }
        {
          badge ? <Badge count="Live Orders Is Receiving" style={{ backgroundColor: '#52c41a' }} /> : <Badge count= "Login To Get Live Orders" style={{ backgroundColor: 'red' }} />
        }
        {
          <Modal
            title="New Order With TimeOut OF 30 Seconds"
            visible={this.state.modalVisible}
            onOk={this.acceptOrder}
            onCancel={this.cancelOrder}
            okText = "Accept"
            cancelText = "Reject"
          >
            <table className = "table_design">
              <tbody className = "table_body">
                <tr>
                  <td className = "table_data">
                    Area :
                  </td>
                  <td className = "table_data">
                    {userarea}
                  </td>
                </tr>
              </tbody>
              <tbody className = "table_body">
                <tr>
                  <td className = "table_data">
                    Items :
                  </td>
                  <td className = "table_data">
                    {items}
                  </td>
                </tr>
              </tbody>
              <tbody className = "table_body">
                <tr>
                  <td className = "table_data">
                    Address :
                  </td>
                  <td className = "table_data">
                    {address}
                  </td>
                </tr>
              </tbody>
              <tbody className = "table_body">
                <tr>
                  <td className = "table_data">
                    Quantity :
                  </td>
                  <td className = "table_data">
                    {quantity}
                  </td>
                </tr>
              </tbody>
              <tbody className = "table_body">
                <tr>
                  <td className = "table_data">
                    Rate :
                  </td>
                  <td className = "table_data">
                    {rate}
                  </td>
                </tr>
              </tbody>
            </table>
            <Input size="large" placeholder="Amount in Rs" onChange = {this.acceptamount} />
          </Modal>
        }
        <br />
      </div>
    )
  }
}
 

export default liveOrdersComponent
