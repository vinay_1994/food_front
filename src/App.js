import  React ,{Component} from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';


import HomeComponent from './Home/Homecomponent';
import Register from './register/RegisterComponent';
import AuthCheck from './common/authLogin';
import CatererDashboard from './catererDash/CatereDashboardNew';
import OrderConfirmation from './Home/OrderConfirmation';
import SuccessPage from './Home/orderSuccess';


class App extends Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={HomeComponent} />
            <Route path="/dashboard" component={CatererDashboard} />
            <Route exact path="/Register" component={Register} />
            <Route exact path="/order_confirmation" component={OrderConfirmation} />
            <Route exact path="/order_success" component={SuccessPage} />
          </Switch>
        </BrowserRouter> 
      </div>      
    );
  }
}





export default App;