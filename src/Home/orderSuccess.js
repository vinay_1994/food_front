import React from 'react';
import { Alert } from 'antd';


function orderSuccess(props) {
  return (
    <div className="success_notification">
        <Alert
        message="Order Placed Successfully"
        description="Order Has Been Placed successfully ..."
        type="success"
        showIcon
        />
    </div>
  )
}


export default orderSuccess;
