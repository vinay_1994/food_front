import React, { Component } from 'react';
import axios from 'axios';
import URL from '../api';
import  tokenStore  from './../common/tokenStore'; 



class Homeservice extends Component {
  
  static Login(obj,callback){
    axios.post(URL.BASE_URL.concat('login'),obj)
        .then(res=>{
          if(res.data.status === 200){ 
            let data = res.data.message;        
            tokenStore.setToken(data.token)
            callback(false,data)
          }else{  
            callback(true,res.data.message)
          }
        })
        .catch(rej =>{
          callback(true , "Network Error")
        })
            
  }
}

export default Homeservice
