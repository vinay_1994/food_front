import React, { Component  } from 'react';
import axios from 'axios';
import URL from '../api';
import socketio from 'socket.io-client';
import HomeService from './Homeservice';
import { Spin, Alert } from 'antd';




import { Input , Select, Button, Modal, Card, Icon, Avatar, Col, Row } from 'antd';
import 'antd/dist/antd.css';

import './Style.css'



class Homecomponent extends Component {

    constructor(props){
        super(props);
        this.state={
            visible:false,
            Email_ID:"",
            password:"",
            search:"",
            CAT_ID:"",
            area:"",
            searchVisible:false,
            quantity:"",
            item:"",
            rate:"",
            address:"",
            bodycontent : true,
            orderConfirmationModal : false,
            amount : "",
            accepted_caterer : "",
            ACCEPTED_CAT_ID : ""
        }
    }
    

    
    componentWillMount() {
        this.io = socketio(URL.BASE_URL.concat('client'));
    }
    


    showModal = () => {
        this.setState({
          visible: true,
        });
      }


    selectChange = key => value => {
        this.setState({
            [key]:value
        })
        
    }

    searchDialog = ()=>{
        this.setState({
            searchVisible:!this.state.searchVisible
        })
    }

    registerHandler=(e)=>{

        this.props.history.push('/Register');
    }

    //admin login
    handleOk = (e) => {
        this.props.history.push('/dashboard')
        let obj={
            username:this.state.Email_ID,
            password:this.state.password
        }
        HomeService.Login(obj, async (err,result)=>{
            if(!err){
                console.log(err)
                this.props.history.push('/dashboard')
            }else{
                console.log(err)
            }

        })
      }

      search = (e)=>{

          this.io.emit('emit',{
              area:this.state.search
          })
      }

      
    
      handleCancel = (e) => {
      
        this.setState({
          visible: false,
        });
      }

      changeHandler= value=>(e)=>{
            this.setState({
                [value]:e.target.value
            })
      }


    //send notification to server with details
    orderRequest = () =>{
        
        const obj = {
            area:this.state.area,
            item:this.state.item,
            quantity:this.state.quantity,
            rate:this.state.rate,
            address:this.state.address
        }
        this.io.emit("ORDER_REQUEST",obj);
        this.setState({
            bodycontent : !this.state.bodycontent,
            spin_visible : !this.state.spin_visible
        })
    }

    
  render() {
    const { TextArea } = Input;
    const Option = Select.Option;
    const { Meta } = Card;
    const { spin_visible, orderConfirmationModal ,accepted_caterer, amount } = this.state;

    this.io.on("ORDER_CONFIRMATION",(data)=>{
        console.log(data)
        this.setState({
            spin_visible : false,
            searchVisible : false,
            accepted_caterer : data.USER_NAME,
            amount : data.amount,
            ACCEPTED_CAT_ID : data.CAT_ID

        })
        this.setState({
            orderConfirmationModal : true
        })
    })
    
    return (
        <div className="main">
            <div className="navbar">
                <Button 
                    type="primary"
                    onClick={this.registerHandler}
                    >
                    REGISTER
                </Button>
                <Button type="primary" onClick={this.showModal}>Login</Button>
                <Button type="primary" onClick={this.searchDialog}>SearchModal</Button>
                <Button type="primary" onClick={this.paypal}>Pay</Button>
            </div>
            {
                orderConfirmationModal ? 
                <Modal
                title="Basic Modal"
                visible={true}
                okText="Pay"
                onOk={this.onPay}
                onCancel={this.handleCancel}
                >
                <p>accepted by caterer<h2> {accepted_caterer} </h2></p>
                <p>with the amount has to be paid <h2>{amount} Rs </h2></p>
                </Modal> : " "
            }
            
            <Modal
            title="Admin Login"
            visible={this.state.visible}
            onOk={this.handleOk}
            onCancel={this.handleCancel}
            bodyStyle={{
                minHeight:"200px",
                minWidth:"300px",
                padding:"50px"
            }}
            >
            <Input placeholder="Email_ID" 
                    onChange={this.changeHandler("Email_ID")}  
                />
                <br/>
                <Input placeholder="password" 
                    onChange={this.changeHandler("password")}  
                />
            </Modal>
            <Modal
            visible={this.state.searchVisible}
            onCancel={this.searchDialog}
            onOk = {this.orderRequest}
            >
            <div className="searchbar">
            {
                spin_visible ? 
                    <Spin tip="Waiting For Order Accepting ...">
                        <Alert
                        message="Order for Caterer"
                        description="Your Order will be accepted within 30 seconds......"
                        type="info"
                        />
                    </Spin> : ""
            }
                <Select
                    showSearch
                    style={{ width: 200 }}
                    placeholder="Area"
                    optionFilterProp="children"
                    onChange={this.selectChange("area")}
                >
                    <Option value="banashankari">Banashankari</Option>
                    <Option value="Jayanagar">Jayanagar</Option>
                    <Option value="J P Nagar">J P Nagar</Option>
                </Select>
                <Select
                    showSearch
                    style={{ width: 200 }}
                    placeholder="Item"
                    optionFilterProp="children"
                    onChange={this.selectChange("item")}
                >
                    <Option value="Dosa">Dosa</Option>
                    <Option value="Vada">Vada</Option>
                    <Option value="Idly">Idly</Option>
                </Select>
                <Select
                    showSearch
                    style={{ width: 200 }}
                    placeholder="within range"
                    optionFilterProp="children"
                    onChange={this.selectChange("rate")}
                >
                    <Option value="100">>100</Option>
                    <Option value="200">>200</Option>
                    <Option value="300">>300</Option>
                </Select>
                <Select
                    showSearch
                    style={{ width: 200 }}
                    placeholder="quantity"
                    optionFilterProp="children"
                    onChange={this.selectChange("quantity")}
                >
                    <Option value="10">>10</Option>
                    <Option value="4">>4</Option>
                    <Option value="2">2</Option>
                </Select>
                <TextArea rows={4} onChange={this.changeHandler("address")} />
                
            </div>
            </Modal>
            
            
    </div>
    )
  }
}


export default Homecomponent
