import React, { Component } from 'react';
import { Spin, Alert } from 'antd';




class OrderConfirmation extends Component {
    constructor(props){
        super(props);
        this.state = {
            spin_visible : true,
        }
    }

  render() {
      console.log(this.props)
      const { spin_visible } = this.state;
    return (
      <div className = "spin">
        {
            spin_visible ? 
            <Spin tip="Waiting For Order Accepting ...">
                <Alert
                message="Order for Caterer"
                description="Your Order will be accepted within 30 seconds......"
                type="info"
                />
            </Spin> : ""
        }
        
      </div>
    )
  }
}


export default OrderConfirmation
