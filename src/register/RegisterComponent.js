import React, { Component } from 'react';
import RegisterService from './registerService';
import './Style.css'

import {
  Form, Input, Tooltip, Icon, Select, Divider , Button, Modal, Alert, message, Popconfirm
} from 'antd';

const { Option } = Select;






class RegisterComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      current: 0,
      user_name:"",
      area:"",
      password:"",
      email:"",
      phone_number:"",
      otpModal : false,
      otp : "",
      successAlert : false,
      showSuccess : false,
      business_name : ""
    };
  }



  changeHandler = value => e =>{
    this.setState({
      [value] : e.target.value
    })
  }

  selectChange = key => value => {
    this.setState({
        [key]:value
    })  
}

  sendOtp = () =>{
    let { otp , email } = this.state;
    const obj ={ otp,email }
    RegisterService.OtpCheck(obj,(err,data)=>{
      if(!err){
        this.setState({
          otpModal : false,
          successAlert : true,
          showSuccess : true
          
        })
      }else{
        this.setState({
          otpModal : false,
          showSuccess : true,
          successAlert : false
        })
        setTimeout(() => {
          window.location.reload()
        }, 4000);
        
      }
    })
  }


  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        const { user_name,phone_number,email,password,area } = this.state
        let obj = {
          user_name,area,phone_number,email,password
        }
        RegisterService.Register(obj,(err,data)=>{
          if(!err){
            this.setState({
              otpModal : true
            })
          }else{
            console.log(data)
          }
        })
      }
    });
  }

  handleConfirmBlur = (e) => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  }

  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  }

  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  }

  handleWebsiteChange = (value) => {
    let autoCompleteResult;
    if (!value) {
      autoCompleteResult = [];
    } else {
      autoCompleteResult = ['.com', '.org', '.net'].map(domain => `${value}${domain}`);
    }
    this.setState({ autoCompleteResult });
  }

  onOk = ()=>{
    window.location.replace('/')
  }

  


  render() {
    const { getFieldDecorator } = this.props.form;
    const { successAlert, showSuccess } = this.state;

    const formItemLayout = {
      labelcol: {
        xs: { span: 12 },
        sm: { span: 4 },
      },
      wrappercol: {
        xs: { span: 12 },
        sm: { span: 8 },
      },
    };
    const tailFormItemLayout = {
      wrappercol: {
        xs: {
          span: 12,
          offset: 0,
        },
        sm: {
          span: 8,
          offset: 4,
        },
      },
    };
    const prefixSelector = getFieldDecorator('prefix', {
      initialValue: '86',
    })(
      <Select style={{ width: 70 }}>
        <Option value="86">+91</Option>
        <Option value="87">+92</Option>
      </Select>
    );
    
  
    return (
      
    <div className="registercomponent">
      <div className="registration_header">UrbanPan</div>
      <Divider />
      <Form {...formItemLayout} onSubmit={this.handleSubmit}>
        <Form.Item
          label="E-mail"
        >
          {getFieldDecorator('email', {
            rules: [{
              type: 'email', message: 'The input is not valid E-mail!',
            }, {
              required: true, message: 'Please input your E-mail!',
            }],
          })(
            <Input onChange={this.changeHandler("email")} />
          )}
        </Form.Item>
        <Form.Item
          label="Password"
        >
          {getFieldDecorator('password', {
            rules: [{
              required: true, message: 'Please input your password!',
            }, {
              validator: this.validateToNextPassword,
            }],
          })(
            <Input type="password" onChange={this.changeHandler("password")}/>
          )}
        </Form.Item>
        <Form.Item
          label="Confirm Password"
        >
          {getFieldDecorator('confirm', {
            rules: [{
              required: true, message: 'Please confirm your password!',
            }, {
              validator: this.compareToFirstPassword,
            }],
          })(
            <Input type="password" onBlur={this.handleConfirmBlur} onChange={this.changeHandler("password")}/>
          )}
        </Form.Item>
        <Form.Item
          label={(
            <span>
              User Name&nbsp;
              <Tooltip title="This will be your display name?">
                <Icon type="question-circle-o" />
              </Tooltip>
            </span>
          )}
        >
          {getFieldDecorator('First Name', {
            rules: [{ required: true, message: 'Please input your First Name!', whitespace: true }],
          })(
            <Input onChange={this.changeHandler("user_name")}/>
          )}
        </Form.Item>
        <Form.Item
          label={(
            <span>
              Area&nbsp;
              <Tooltip title="This will be your display name?">
                <Icon type="question-circle-o" />
              </Tooltip>
            </span>
          )}
        >
          {getFieldDecorator('Last Name', {
            rules: [{ required: true, message: 'Please input your Last Name!', whitespace: true }],
          })(
            <Select
                    showSearch
                    placeholder="Area"
                    optionFilterProp="children"
                    onChange={this.selectChange("area")}
                >
                    <Option value="banashankari">Banashankari</Option>
                    <Option value="Jayanagar">Jayanagar</Option>
                    <Option value="J P Nagar">J P Nagar</Option>
                </Select>
          )}
        </Form.Item>
        <Form.Item
          label={(
            <span>
              Busseness Name&nbsp;
              <Tooltip title="This will be your displaying business name ">
                <Icon type="question-circle-o" />
              </Tooltip>
            </span>
          )}
        >
          {getFieldDecorator('Busseness Nmae', {
            rules: [{ required: true, message: 'Please input your First Name!', whitespace: true }],
          })(
            <Input onChange={this.changeHandler("business_name")}/>
          )}
        </Form.Item>
        <Form.Item
          label="Phone Number"
        >
          {getFieldDecorator('phone', {
            rules: [{ required: true, message: 'Please input your phone number!' }],
          })(
            <Input addonBefore={prefixSelector}  onChange={this.changeHandler("phone_number")} />
          )}
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          {getFieldDecorator('agreement', {
            valuePropName: 'checked',
          })}
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit">Register</Button>
        </Form.Item>
      </Form>
      {
        <Modal
        title="Basic Modal"
        visible= {this.state.otpModal}
        onOk={this.sendOtp}
        onCancel={this.handleCancel}
      >
        <Input size="large" placeholder="large size" onChange = {this.changeHandler("otp")}/>
      </Modal>
      }
      { showSuccess ?  successAlert ? 
        Modal.success({
          title: 'Success',
          content: 'OTP has been verified success fully...',
          okText : "Click To Login",
          onOk:this.onOk
        }).null :
      
        Modal.error({
          title: 'Error',
          content: 'Invalid Otp...',
        }).null : " "
      }
    </div>
    )
  }
}

const WrappedRegistrationForm = Form.create({ name: 'register' })(RegisterComponent);

export default WrappedRegistrationForm
