import React, { Component } from 'react';
import axios from 'axios';
import URL from './../api'

export default class registerService extends Component {
  
    static Register(obj,cb){
        axios.post(URL.BASE_URL.concat(URL.REGISTER),obj)
        .then(res=>{
            if(res.data.status === 200){
                cb(false,res.data)
            }else{
                cb(true,res.data)
            }
        })
        .catch(rej=>{
            cb(true,rej.data)
        })
    }

    static OtpCheck(obj,cb){
        axios.post(URL.BASE_URL.concat(URL.VERIFY_OTP),obj)
        .then(res=>{
            if(res.data.status===200){
                cb(false,res.data)
            }else{
                cb(true,res.data);
            }
        })
        .catch(rej=>{
            cb(true,rej.data)
        })
    }
}
