'use strict'

import tokenStore from './tokenStore';

class Interceptor {

    requestInterceptor = axios => {
      // Add a request interceptor
      axios.interceptors.request.use(
        config => {

          const token = tokenStore.getToken()
          if (!token) {
            //sendToLoginPage();
            // window.location.replace('/');
            window.location.reload();
            return {};
          }
          // config.headers['x-auth-token'] = token;
          config.headers.Authorization = token;
          return config;
        },
        // eslint-disable-next-line
        error => {
          // console.log(`REQI Error:${JSON.stringify(error, null, 2)}`);
          return Promise.reject(error);
        }
      );
    };
  
    responseInterceptor = axios => {
      // Add a response interceptor
      axios.interceptors.response.use(
        response => {
          // console.log('errorStatus', JSON.stringify(response));
          if (
            response &&
            response.data &&
            response.data.status &&
            response.data.status === 401
          ) {
            //sendToLoginPage();
          }
  
          return response;
        },
        error => {
          // console.log(error);
          // console.log('Filter Error:', JSON.stringify(error));
  
          // eslint-disable-next-line
          const { status, config } = error.response;
          // eslint-disable-next-line
          /* if (status && status === UNAUTHORIZED && config && config.__isRetryRequest) {
            sendToLoginPage();
          }
          return Promise.reject(error); */
        }
      );
    };
  }

  
  const applyFilter = axios => {   
      const filter = new Interceptor();
      filter.requestInterceptor(axios);
      filter.responseInterceptor(axios);
    }
  
  const cancleRequest = call => {
    console.log(call);
  };
  
  export default applyFilter;
  export { cancleRequest };