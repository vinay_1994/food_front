import React from 'react';
import {
    Route,
    Redirect,
  } from "react-router-dom";


  function isLogedin(){
      const token = sessionStorage.getItem("_token_");
      if(token){
          return true;
      }else{
          return false;
      }
  }

function authLogin({ component: Component, ...rest }) {
  return (
    <Route 
    {...rest}
    render = { props => 
    isLogedin() ? (
        <Component {...props} />
    ) : (
        <Redirect 
            to = {{
                pathname : "/"
            }}
        />
    )}


    />
  )
}

export default authLogin
