import JWT from 'jsonwebtoken'


const TOKEN_STORE = {
     getToken : () => {
        try {
            return sessionStorage.getItem("_token_")
        } catch (error) {
            return null
        }
    },
    
    setToken : (token) => {
    
        try {
            sessionStorage.setItem("_token_", token);
        } catch (error) {
            console.log(error)
        }
    
    },
    
    decodeToken : token =>{
        return JWT.decode(token)
    }
}


export default TOKEN_STORE


    
  


