import React, { Component, createContext } from 'react';
import Dashboard from './../catererDash/CatereDashboardNew';
import LiveOrders from './../catererDash/liveOrdersComponent';
import JWT from './../common/tokenStore';


const { Provider, Consumer } = createContext();

class contextApi extends Component {
    constructor(props){
        super(props);
        this.state = {
            CAT_ID : "",
            USER_NAME : "",
            AREA : ""
        }
    }

    componentDidMount() {
        let token = JWT.decodeToken(JWT.getToken());
        console.log(token)
    }
    


  render() {
    return (
      <Provider value = {this.state}>
        <Dashboard />
        <LiveOrders />
      </Provider>
    )
  }
}

export default { contextApi, Consumer }
